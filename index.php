<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>PHP SC S5: Activity: Client-Server Communication</title>
</head>
<body>
	<?php session_start();?>

	<?php if(!isset($_SESSION['username'])): ?>
		<form method="POST" action="./server.php">
		<input type="hidden" name="action" value="login"/>
		Email: <input type="email" name="username" required/>
		Password: <input type="password" name="password" required/>
		<button type="submit">Login</button>
		</form>

	<?php else: ?>
		<p>Hello, <?= $_SESSION['username']; ?></p>
		<form method="POST" action="./server.php">
		<input type="hidden" name="action" value="logout">
		<button type="submit">Logout</button>		
	</form>
	<?php endif; ?>
	

</body>
</html>